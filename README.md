# Fundamentos de Programación con JavaScript

## Ejercicios

### Suma de 2 dígitos

Dado un número de 2 dígitos *(num)*. Retornar la suma de sus dígitos.

- Ejemplo

Para ```num = 29```, la salida de la función ```sumaDosDigitos(n) = 11```.

- Input/Output

[límite de tiempo] 4000ms (js)

[input] integer num

Un número entero positivo.

**Constraints:**

10 ≤ num ≤ 99.

[output] integer

La suma del primer y segundo dígito de un número ingresado.